/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout.storm.main;

/**
 *
 * @author lk
 */
public class Constants {
    
    public static final String ACTIVEMQ_BROKER_URL = "tcp://tarkk.com:61616?keepAlive=true";
    public static final String ACTIVEMQ_QUEUE_NAME = "blackout.tweets";
    
    public static final String SEARCH_GRAPH_URL = "http://tarkk.com:8080/SearchGraph/api/graph";
    public static final String FREEBASE_SEARCH_URL = "https://www.googleapis.com/freebase/v1/search";
    public static final String SOLR_URL = "http://tarkk.com:8983/solr/";
    
    
    public static final String BLACKOUT_SPOUT = "BLACKOUT_SPOUT";
    public static final String BOLT_TAGGER = "BOLT_TAGGER";
    public static final String BOLT_SOLR_LOOKUP = "BOLT_SOLR_LOOKUP";
    public static final String BOLT_FREEBASE_LOOKUP = "BOLT_FREEBASE_LOOKUP";
    public static final String BOLT_GRAPH_LOOKUP = "BOLT_GRAPH_LOOKUP";
    public static final String BOLT_CALCULATE_RANK = "BOLT_CALCULATE_RANK";
    
}
