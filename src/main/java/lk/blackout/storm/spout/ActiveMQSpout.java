/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout.storm.spout;

import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import lk.blackout.storm.main.Constants;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author lk
 */
public class ActiveMQSpout implements IRichSpout, MessageListener {

    final static Logger logger = LogManager.getLogger(ActiveMQSpout.class.getName());
    //queue for storing messages from JMS
    private LinkedBlockingQueue<Message> queue;
    //for storing messages currently being processed for acking later
    private ConcurrentHashMap<String, Message> pendingMessages;
    private SpoutOutputCollector collector;
    private transient Connection connection;
    private transient Session session;

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.queue = new LinkedBlockingQueue<>();
        this.pendingMessages = new ConcurrentHashMap<>();
        this.collector = collector;
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
            connectionFactory.setBrokerURL(Constants.ACTIVEMQ_BROKER_URL);

            this.connection = connectionFactory.createConnection();
            this.connection.start();

            this.session = connection.createSession(false, ActiveMQSession.AUTO_ACKNOWLEDGE);
            Destination dest = session.createQueue(Constants.ACTIVEMQ_QUEUE_NAME);
//            Destination dest = session.createQueue("TestQueue");
            MessageConsumer consumer = session.createConsumer(dest);
            consumer.setMessageListener(this);

        } catch (JMSException ex) {
            logger.error("error connecting to activemq: {}", ex.getMessage());
        }
    }

    @Override
    public void onMessage(Message msg) {
        TextMessage textMessage = (TextMessage) msg;
        try {
            logger.info("Queuing msgId [" + msg.getJMSMessageID() + "]");
            logger.info("Queuing msg [" + textMessage.getText() + "]");
        } catch (JMSException e) {
            logger.error(e);
        }
        this.queue.offer(msg);

    }

    @Override
    public void close() {
        try {
            logger.trace("Closing JMS connection.");
            this.session.close();
            this.connection.close();
        } catch (JMSException e) {
            logger.error(e);
        }
    }

    @Override
    public void activate() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deactivate() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void nextTuple() {
        Message msg = this.queue.poll();
        if (msg == null) {
            Utils.sleep(100);
        } else {
            logger.info("sending tuple: " + msg);
            // get the tuple from the handler
            try {
                //get the text from the msg and convert it to tuple. 
                //@lk Maybe check for valid message?
                String tweetJson;
                if (msg instanceof TextMessage) {
                    tweetJson = ((TextMessage) msg).getText();
                } else {
                    tweetJson = null;
                }
                Values vals = new Values(tweetJson);
                this.collector.emit(vals, msg.getJMSMessageID());
                //save message for acking later
                this.pendingMessages.put(msg.getJMSMessageID(), msg);
            } catch (JMSException e) {
                logger.info("Unable to convert JMS message: " + msg);
            }
        }
    }

    @Override
    public void ack(Object msgId) {
        logger.info("Ack msgId : " + msgId);
        Message msg = this.pendingMessages.remove((String) msgId);
        if (msg != null) {
//            try {
//                msg.acknowledge();
//                LOG.info("JMS Message acked: " + msgId);
//            } catch (JMSException e) {
//                LOG.error(e);
//            }
        } else {
            logger.info("Couldn't acknowledge unknown JMS message ID: " + msgId);
        }
    }

    @Override
    public void fail(Object msgId) {
        logger.warn("Message failed: " + msgId);
        Message msg = this.pendingMessages.get((String) msgId);
        this.pendingMessages.remove((String) msgId);
        String msgText;
        try {
            if (msg != null && msg instanceof TextMessage) {
                msgText = ((TextMessage) msg).getText();
                if (msgText != null) {
                    sendMessage(msgText);
                    logger.warn("failed message sent to the queue. " + msgText);
                } else {
                    logger.warn("error saving failed message to the queue.");
                }
            } else {
                logger.warn("the message is not typeof TextMessage");
            }
//            session.recover();
//            LOG.warn("Recovering messageId : " + msgId);
        } catch (JMSException ex) {
            logger.warn("Error sending message to the queue : " + msgId);
        }
    }

    public void sendMessage(String msg) {
        try {
            Session session = connection.createSession(false, ActiveMQSession.INDIVIDUAL_ACKNOWLEDGE);

            // Create the destination
            Destination destination = session.createQueue("blackout.failed.queue");
//            Destination destination = session.createQueue("TestQueue");

            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);

            // Create a messages            
            TextMessage message = session.createTextMessage(msg);

            // Tell the producer to send the message
            producer.send(message);
            session.close();
        } catch (Exception e) {
            logger.info("error sending message(" + msg + ") to queue", e);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweetJson"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config ret = new Config();
        return ret;
    }

}
